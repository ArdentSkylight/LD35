﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{
    protected StateMachine stateMachine = new StateMachine();

    public delegate void OnDeathEventAction(Unit unit);
    public event OnDeathEventAction OnDeathEvent;

    [Header("Unit Properties")]
    public float maxHealth = 100;
    [HideInInspector]
    public float health;
    public int movementDistance = 1;

    public Vector3 actualPosition;

    void Awake()
    {
        health = maxHealth;

        actualPosition = transform.position;

        stateMachine.PushState(StateIdle);
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
        actualPosition = position;
    }

    public virtual void TakeDamage(float amount)
    {
        health -= amount;

        if (health <= 0)
            OnDeath();
    }

    protected virtual void OnDeath()
    {
        GameMaster.enemyKills++;
        Debug.Log("Enemies killed: " + GameMaster.enemyKills);

        if (OnDeathEvent != null)
            OnDeathEvent(this);

        Destroy(gameObject);
    }

    protected virtual void Update()
    {
        transform.position = Vector3.Lerp(transform.position, actualPosition, GameMaster.instance.BPM * Time.deltaTime * 0.1f);
    }

    protected virtual void StateIdle()
    {

    }

    void OnEnable()
    {
        GameMaster.OnBeat += OnBeat;
    }

    void OnDisable()
    {
        GameMaster.OnBeat -= OnBeat;
    }

    void OnBeat()
    {
        stateMachine.Cycle();
    }
}
