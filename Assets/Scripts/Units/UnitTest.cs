﻿using UnityEngine;
using System.Collections;

public class UnitTest : Unit {

    protected override void StateIdle()
    {
        base.StateIdle();

        int decision = Random.Range(0, 1);

        if (decision == 0)
            stateMachine.PushState(StateMoveRandom);

        stateMachine.Cycle(); //After deciding on an action, run it once.
    }

    void StateMoveRandom()
    {
        if (Random.Range(0, 2) == 0)
            actualPosition += new Vector3((Mathf.Round(Random.value) * 2 - 1) * movementDistance, 0, 0);
        else
            actualPosition += new Vector3(0, 0, (Mathf.Round(Random.value) * 2 - 1) * movementDistance);

        TakeDamage(25);

        stateMachine.PopState(); //Use this to go back to the Idle state.
    }
}
