﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

    public struct ShakeInformation
    {
        public ShakeInformation(float _power, float _duration, float _interval)
        {
            power = _power;
            duration = _duration;
            interval = _interval;
        }

        public float power;
        public float duration;
        public float interval;
    }

    public static CameraManager instance;

    [Header("Camera Properties")]
    public Vector3 defaultPosition;
    public float speed = 3f;
    public float followHeight = 10f;
    public bool followPlayer;

    private Vector3 shakeOffset;

    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (followPlayer && Player.instance != null)
        {
            transform.position = Vector3.Lerp(transform.position, Player.instance.transform.position + Vector3.up * followHeight + Vector3.back * 4, Time.deltaTime * speed) + shakeOffset;
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, defaultPosition, Time.deltaTime * speed) + shakeOffset;
        }
    }

    public void ShakeScreen(float power, float duration, float interval = 0.25f)
    {
        StopCoroutine("Shake");
        shakeOffset = Vector3.zero;

        StartCoroutine("Shake", new ShakeInformation(power, duration, interval));
    }

    IEnumerator Shake(ShakeInformation info)
    {
        while (info.duration > 0)
        {
            float shakeX = (Random.value * 2 - 1) * info.power;
            float shakeY = (Random.value * 2 - 1) * info.power;
            shakeOffset = new Vector3(shakeX, 0, shakeY);

            yield return new WaitForSeconds(info.interval);
            info.duration -= info.interval;
        }

        shakeOffset = Vector3.zero;
    }
}
