﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomManager : MonoBehaviour {

    public static RoomManager instance;

    public static Room currentRoom;
    public static int currentRoomIndex;

    [Header("Room Manager Properties")]
    public Unit[] enemyPrefabs;
    public Room[] roomPrefabs;

    void Awake()
    {
        instance = this;

        if (roomPrefabs.Length > 0 && roomPrefabs[0] != null)
        {
            LoadRoom(0);
        }
        else
        {
            Debug.LogWarning("No starting room exists in RoomManager.");
        }
    }

    public void NextRoom()
    {
        if (currentRoomIndex + 1 < roomPrefabs.Length)
        {
            LoadRoom(currentRoomIndex + 1);
            GameMaster.clearedRooms++;
            Debug.Log("Rooms cleared: " + GameMaster.clearedRooms);
        }
    }

    public void LoadRoom(int index)
    {
        if (index >= 0 && index < roomPrefabs.Length && roomPrefabs[index] != null)
        {
            if (currentRoom != null)
            {
                currentRoom.Unload();
                Destroy(currentRoom.gameObject);
            }

            currentRoom = Instantiate(roomPrefabs[index], Vector3.zero, Quaternion.identity) as Room;

            currentRoom.Load();
            Player.instance.SetPosition(currentRoom.elevatorPosition);

            currentRoomIndex = index;
        }
        else
        {
            Debug.LogWarning("Trying to load room #" + index + " but it doesn't exist.");
        }
    }

    public Unit GetRandomUnitPrefab()
    {
        if (enemyPrefabs.Length > 0)
        {
            return enemyPrefabs[Random.Range(0, enemyPrefabs.Length)];
        }
        else
        {
            Debug.LogWarning("Trying to get a random unit prefab from RoomManager but none exist.");
            return null;
        }
    }
}
