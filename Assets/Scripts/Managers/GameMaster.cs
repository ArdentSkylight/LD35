﻿using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour {

    public static GameMaster instance;

    public delegate void BeatAction();
    public static event BeatAction OnBeat;

    public static float lastBeatTimestamp;
    public static float nextBeatTimestamp;

    public static int clearedRooms;
    public static int enemyKills;

    [Header("Beat Properties")]
    public int BPM;
    [SerializeField]
    private AudioClip beatSound;
    [SerializeField]
    private bool mute;

    private float beatTimer;

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(transform.parent);

        float beatsPerSecond = 60f / BPM;

        nextBeatTimestamp = beatsPerSecond;
    }

    void Update()
    {
        //Beat
        float beatsPerSecond = 60f / BPM;
        if (beatTimer >= beatsPerSecond)
        {
            if(beatSound != null && !mute)
                AudioManager.instance.PlaySound(beatSound, Vector3.zero);
            if(OnBeat != null)
                OnBeat();

            lastBeatTimestamp = nextBeatTimestamp;
            nextBeatTimestamp = Time.time + beatsPerSecond;

            beatTimer = 0;
        }

        beatTimer += Time.deltaTime;
    }
}
