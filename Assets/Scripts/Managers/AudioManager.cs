﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;

	[Range(0f, 1f)]
	public float masterVolume = 1f;
	[Range(0f, 1f)]
	public float sfxVolume = 1f;
	[Range(0f, 1f)]
	public float musicVolume = 1f;

	private AudioSource musicSource;

    AudioManager()
    {
        instance = this;
    }

	void Awake()
	{
        GameObject musicObject = new GameObject("Music Source");
        musicObject.transform.parent = transform;

        musicSource = musicObject.AddComponent<AudioSource>();
    }

	public void PlaySound(AudioClip clip, Vector3 position)
	{
		AudioSource.PlayClipAtPoint(clip, position, sfxVolume * masterVolume);
	}

	public void PlayMusic(AudioClip clip, float fadeOutDur, float fadeInDur, bool loop)
	{
		StartCoroutine(StartMusic(clip, fadeOutDur, fadeInDur, loop));
	}

	IEnumerator StartMusic(AudioClip clip, float fadeOutDur, float fadeInDur, bool loop)
	{
		StopCoroutine("HandleMusic");

		// Fade out if current music is playing
		if(musicSource.isPlaying && fadeOutDur > 0f)
		{
			float fadePercent = 1f;

			while(fadePercent > 0f)
			{
				yield return null;
				fadePercent -= Time.deltaTime / fadeOutDur;
				musicSource.volume = fadePercent * masterVolume * musicVolume;
			}
		}

		musicSource.Stop();

		// Update music clip
		musicSource.clip = clip;
		musicSource.loop = loop;

		musicSource.Play();

		// Fade in new music
		if(fadeInDur > 0f)
		{
			musicSource.volume = 0f;
			float fadePercent = 0f;

			while(fadePercent < 1f)
			{
				yield return null;
				fadePercent += Time.deltaTime / fadeInDur;
				musicSource.volume = fadePercent * masterVolume * musicVolume;
			}
		}

		StartCoroutine("HandleMusic");
	}

	IEnumerator HandleMusic()
	{
		while(musicSource.isPlaying)
		{
			musicSource.volume = masterVolume * musicVolume;
			yield return null;
		}
	}
}
