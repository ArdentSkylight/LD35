﻿using UnityEngine;
using System.Collections;

public class Projectile : Team
{
    public int travelSpeed;
    public int travelDistance;
    public Vector3 travelDirection;
    
    public Projectile(int travelSpeed, int travelDistance, Vector3 travelDirection)
    {
        this.travelSpeed = travelSpeed;
        this.travelDistance = travelDistance;
        this.travelDirection = travelDirection;
    }

    public void UpdateProjectilePerBeat()
    {
        Debug.Log("Updating projectile");
        //Movement
        transform.position = Vector3.Lerp(transform.position, transform.position + (travelDirection * travelSpeed), Static.ACTIONDURATION);

        travelDistance -= travelSpeed;

        //Check collision
        if (true)
        {//if check for collisions
            //Wall
            //Unit
            foreach (GameObject go in UnitManager.listUnits)
            {
                if (IsTeamEnemy(go) && transform.position == go.transform.position)
                {//Collision, explode / deal damage to player, and destory this game object
                    Debug.Log("Projectile" + name + " collided with " + go.name);
                }
                //else if()//Collision with terrain
                {

                }
            }
        }

        //Enemy team

        if (travelDistance < 1)
        {//Check for collisions and, Destroy this projectile

        }
    }

    

    void OnEnable()
    {
        GameMaster.OnBeat += OnBeat;
    }
    void OnDisable()
    {
        //GameMaster.OnBeat += OnBeat;//I assume this is supposed to be -=
        GameMaster.OnBeat -= OnBeat;
    }
    void OnBeat()
    {
        UpdateProjectilePerBeat();
    }
}
