﻿using UnityEngine;
using System.Collections;

public static class Static
{
    public static float TURNDURATION = 1.5f;
    public static float ACTIONDURATION = 1.0f;
    public static SpellManager sm;//Script spell manager

    public enum Direction
    {
        DNone = 0,
        DUp,
        DDown,
        DLeft,
        DRight
    }
    public enum Actions//Enemy ai can have one action
    {
        ANone = 0,
        AMove,
        AAttack,
        ASpell,
        AAmount//This is used to know how many ai actions there add more ai actions above, not bellow this
    }
}
