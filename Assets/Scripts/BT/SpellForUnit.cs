﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpellForUnit : MonoBehaviour
{
    public List<Spell> listSpells;

    public void Start()
    {
        listSpells = new List<Spell>();
        listSpells.Add(new Spell(5, SpellManager.SpellID.SIDFireball));
    }

    public void CastSpellID(int SpellListID, GameObject caster, Vector3 TargetLocation)
    {
        if (SpellListID < listSpells.Count)
            listSpells[SpellListID].Cast(caster, TargetLocation);
        else
            Debug.Log("Casting spell out of range");
    }

    void UpdateSpellsPerBeat()
    {
        foreach(Spell s in listSpells)
        {
            s.UpdateSpellsPerBeat();
        }
    }

    void OnEnable()
    {
        GameMaster.OnBeat += OnBeat;
    }
    void OnDisable()
    {
        //GameMaster.OnBeat += OnBeat;//I assume this is supposed to be -=
        GameMaster.OnBeat -= OnBeat;
    }
    void OnBeat()
    {
        UpdateSpellsPerBeat();
    }
}
