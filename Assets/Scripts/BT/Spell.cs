﻿using UnityEngine;
using System.Collections;

public class Spell
{
    int cooldownDuration;//Time it takes for spell to come off cooldown, beats duration, not seconds
    int currentCooldown;//Beats duration, not seconds
    SpellManager.SpellID spellID;

    public Spell(int cooldown, SpellManager.SpellID spellID)
    {
        cooldownDuration = cooldown;
        currentCooldown = 0;
        this.spellID = spellID;
    }

    public void Start()
    {
        cooldownDuration = 2;
        currentCooldown = 0;
        spellID = SpellManager.SpellID.SIDFireball;
    }

    public void UpdateSpellsPerBeat()
    {
        Debug.Log("updating spells");
        if (currentCooldown != 0)
            currentCooldown--;
    }

    public void Cast(GameObject caster, Vector3 targetLocation)
    {
        if(currentCooldown == 0)
        {
            Debug.Log("Spells.cs casting spell");
            currentCooldown = cooldownDuration;
            Static.sm.AddPrefab(spellID, caster, targetLocation);
        }
        else
            Debug.Log("Error spell is on cooldown");
        //else error, spell is on cooldown
    }
}
