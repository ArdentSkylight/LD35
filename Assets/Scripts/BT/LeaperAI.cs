﻿using UnityEngine;
using System.Collections;

public class LeaperAI : AIForUnit
{

    public override void DecideAction()
    {
        //Move towards player until in attack range, then attack. Verry simple
        if (IsInRangeToAttack())
            Attack();
        else
            Move();
    }

    public override void Attack()
    {
        //Attacks player
    }
    public override void Move()
    {
        //if health is low, occasionally runs away from player
            //Chance to trigger leap, moves 2 tiles instead of one, if position is not occupied
        //Else moves towards player
    }
    public override void CastSpell()
    {

    }
}
