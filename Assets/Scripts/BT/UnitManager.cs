﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitManager : MonoBehaviour
{
    public float accumulatedTime = 0f;
    public Object enemy;
    public Object player;

    public static List<GameObject> listUnits;
    private GameObject obj;


    // Use this for initialization
    void Start ()
    {
        accumulatedTime = 0f;
        listUnits = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        accumulatedTime += Time.deltaTime;
        if (accumulatedTime > Static.TURNDURATION)
        {//Every TURNDURATION trigger ai TakeAction
            accumulatedTime -= 1.0f;
            for (int i = 0; i < listUnits.Count; i++)
            {
                AIForUnit ai = listUnits[i].GetComponent<AIForUnit>();
                ai.DecideAction();
            }
        }

        //Code to add and capture units
        if (Input.GetKeyDown(KeyCode.Space))
        {
            int randAmount = Random.Range(0, 10);
            for (int i = 0; i < randAmount; i++)
            {
                obj = Instantiate(enemy, Vector3.zero, new Quaternion(0, 0, 0, 0)) as GameObject;
                listUnits.Add(obj);
            }
        }
        //Code to add and capture player
        if (Input.GetKeyDown(KeyCode.P))
        {
            obj = Instantiate(player, Vector3.zero, new Quaternion(0, 0, 0, 0)) as GameObject;
            listUnits.Add(obj);
        }
    }
}
