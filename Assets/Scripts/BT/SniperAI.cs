﻿using UnityEngine;
using System.Collections;

public class SniperAI : AIForUnit
{
    public override void DecideAction()
    {
        //Move towards player until in attack range, then attack. Verry simple
        if (IsInRangeToAttack())
            Attack();
        else
            Move();
    }

    public override void Attack()
    {
        //Attacks player
    }
    public override void Move()
    {
        //if health is low, doesn't run away from player
        //Else moves towards player
        transform.position = Vector3.Lerp(transform.position, PlayerUnit.position, Static.ACTIONDURATION * Time.deltaTime);
    }
    public override void CastSpell()
    {

    }
}
