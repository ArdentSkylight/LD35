﻿using UnityEngine;
using System.Collections;

public class Team : MonoBehaviour
{
    public enum TeamType
    {
        TTPlayer = 0,
        TTEnemy
    }

    public TeamType teamType;

    //IsTeamEnemy
    public bool IsTeamEnemy(TeamType TeamType)
    {
        if (teamType == TeamType)//Same team is not enemy
            return false;
        return true;//Else its enemy
    }
    public bool IsTeamEnemy(GameObject go)
    {
        if (teamType == go.GetComponent<Team>().teamType)//Same team is not enemy
            return false;
        return true;//Else its enemy
    }


    //IsTeamAlly
    public bool IsTeamAlly(TeamType TeamType)
    {
        if (teamType == TeamType)//Same team is not enemy
            return true;
        return false;//Else its enemy
    }
    public bool IsTeamAlly(GameObject go)
    {
        if (teamType == go.GetComponent<Team>().teamType)//Same team is not enemy
            return true;
        return false;//Else its enemy
    }
}
