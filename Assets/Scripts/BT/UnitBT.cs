﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitBT : Team
{
    public int health;
    public float attackRange;
    public float movementSpeed;
    public Vector3 actualPosition;
    public int damage;
    public List<Vector3> listPath;

	// Use this for initialization
	void Start ()
    {
        health = 3;
        attackRange = 1;
        movementSpeed = 1;
        actualPosition = transform.position;
        damage = 1;
	}

    public void TakeDamage(int damage)
    {
        health -= damage;
        if(health < 1)
        {
            Debug.Log(name + "Got killed");
            gameObject.SetActive(false);
        }
    }


    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, actualPosition, GameMaster.instance.BPM * Time.deltaTime * 0.1f);
    }
}
