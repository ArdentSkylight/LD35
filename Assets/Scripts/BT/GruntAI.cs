﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class GruntAI : AIForUnit
{
    public override void DecideAction()
    {
        //Move towards player until in attack range, then attack. Verry simple
        if (IsInRangeToAttack())
            Attack();
        else
            Move();
    }

    public override void Attack()
    {
        //Attacks player
        Debug.Log(name + " attacks");
        Player.instance.GetComponent<Unit>().TakeDamage(GetComponent<UnitBT>().damage);
    }
    public override void Move()
    {
        UnitBT u = GetComponent<UnitBT>();
        if (u.listPath.Count < 1)
            Pathfinding.instance.FindClosestPath(transform.position, Player.instance.transform.position, u.listPath);
            
        u.actualPosition = transform.position + (u.listPath.Last() * GetComponent<UnitBT>().movementSpeed);
    }
    public override void CastSpell()
    {

    }
}
