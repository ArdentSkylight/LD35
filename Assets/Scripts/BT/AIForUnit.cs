﻿using UnityEngine;
using System.Collections;

public class AIForUnit : MonoBehaviour
{
    public virtual void DecideAction()
    {

    }

    public bool IsInRangeToAttack()
    {
        if(Mathf.Abs(transform.position.x - PlayerUnit.position.x) + Mathf.Abs(transform.position.z - PlayerUnit.position.z) <= GetComponent<UnitBT>().attackRange)
            return true;
        return false;
    }

    public virtual void Attack()
    {

    }

    public virtual void Move()
    {

    }

    public virtual void CastSpell()
    {

    }

    void OnEnable()
    {
        GameMaster.OnBeat += OnBeat;
    }
    void OnDisable()
    {
        //GameMaster.OnBeat += OnBeat;//I assume this is supposed to be -=
        GameMaster.OnBeat -= OnBeat;
    }
    void OnBeat()
    {
        DecideAction();
    }
}
