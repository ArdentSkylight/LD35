﻿using UnityEngine;
using System.Collections;

public class PlayerUnit : MonoBehaviour
{
    public float movementDuration = 0.7f;
    public float movementDistance = 100;
    public Static.Actions action;

    public static Vector3 position;

	// Use this for initialization
	void Start ()
    {
        movementDuration = 0.7f;
        movementDistance = 100f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        //TEMPORARY REMOVE AFTER TESTING
        if (action == Static.Actions.ANone)
            action = (Static.Actions)(Random.Range(0, (int)Static.Actions.AAmount));

        position = transform.position;

        //Movement
        if (Input.GetKeyDown(KeyCode.W))
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, 0, movementDistance), movementDuration * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, 0, -movementDistance), movementDuration * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(-movementDistance, 0, 0), movementDuration * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(movementDistance, 0, 0), movementDuration * Time.deltaTime);
        }
    }
}
