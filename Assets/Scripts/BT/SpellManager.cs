﻿using UnityEngine;
using System.Collections;

public class SpellManager : MonoBehaviour
{
    public enum SpellID
    {
        SIDFireball = 0
    }


    public GameObject FireballSpellPrebaf;

	// Use this for initialization
	public void Start ()
    {
        Static.sm = this;
	}

    public void AddPrefab(SpellID spellID, GameObject caster, Vector3 targetLocation)
    {
        switch (spellID)
        {
            case SpellID.SIDFireball:
                GameObject go = Instantiate(FireballSpellPrebaf, caster.transform.position, Quaternion.identity) as GameObject;
                go.transform.LookAt(targetLocation);
                go.GetComponent<Team>().teamType = caster.GetComponent<Team>().teamType;
                break;

            default:

                break;
        }

    }

}
