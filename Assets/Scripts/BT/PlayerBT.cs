﻿using UnityEngine;
using System.Collections;

public class PlayerBT : MonoBehaviour {

    public enum TimingQuality
    {
        Fail,
        Good,
        Perfect
    }

    public delegate void BeatAction(TimingQuality quality);

    public static PlayerBT instance;

    [HideInInspector]
    public float health = 100;
    [Header("Player Properties")]
    public float maxHealth = 100;
    public int movementDistance = 1;

    [Space]

    [Header("Timing Quality")]
    [Range(0, 1), Tooltip("Success = Timing Delta <= Beats Per Second * Window")]
    public float perfectTimingWindow;
    [Range(0, 1), Tooltip("Success = Timing Delta <= Beats Per Second * Window")]
    public float goodTimingWindow;

    public Vector3 actualPosition { get; private set; }

    private float lastSuccessfullBeat;

    void Awake()
    {
        instance = this;
        actualPosition = transform.position;

        health = maxHealth;
    }

    void Update()
    {
        HandleInput();

        transform.position = Vector3.Lerp(transform.position, actualPosition, GameMaster.instance.BPM * Time.deltaTime * 0.1f);

        GetComponent<MeshRenderer>().material.color = Color.Lerp(GetComponent<MeshRenderer>().material.color, Color.white, Time.deltaTime * 8);
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
        actualPosition = position;
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
            HandleAction(MoveUp);
        else if (Input.GetKeyDown(KeyCode.S))
            HandleAction(MoveDown);
        else if (Input.GetKeyDown(KeyCode.A))
            HandleAction(MoveLeft);
        else if (Input.GetKeyDown(KeyCode.D))
            HandleAction(MoveRight);

        //More input stuffs
    }

    void HandleAction(BeatAction action)
    {
        float BPS = 60f / GameMaster.instance.BPM;
        float timingDeltaLast = Mathf.Abs(GameMaster.lastBeatTimestamp - Time.time); //Delta Last beat -> Timing
        float timingDeltaNext = Mathf.Abs(Time.time - GameMaster.nextBeatTimestamp); //Delta Timing -> Next beat

        TimingQuality quality = TimingQuality.Fail;

        if (lastSuccessfullBeat != GameMaster.lastBeatTimestamp && timingDeltaLast <= timingDeltaNext) //Last beat check
        {
            if (timingDeltaLast <= BPS * (perfectTimingWindow / 2f)) //Perfect
            {
                Debug.Log("*PERFECT* Timing info (LAST BEAT): " + timingDeltaLast + "/" + BPS * (perfectTimingWindow / 2f));
                quality = TimingQuality.Perfect;
            }
            else if (timingDeltaLast <= BPS * (goodTimingWindow / 2f)) //Good
            {
                Debug.Log("*GOOD* timing info (LAST BEAT): " + timingDeltaLast + "/" + BPS * (goodTimingWindow / 2f));
                quality = TimingQuality.Good;
            }

            if (quality != TimingQuality.Fail)
                lastSuccessfullBeat = GameMaster.lastBeatTimestamp;
        }
        else if(lastSuccessfullBeat != GameMaster.nextBeatTimestamp)//Next beat check
        {
            if (timingDeltaNext <= BPS * (perfectTimingWindow / 2f)) //Perfect
            {
                Debug.Log("*PERFECT* Timing info (NEXT BEAT): " + timingDeltaNext + "/" + BPS * (perfectTimingWindow / 2f));
                quality = TimingQuality.Perfect;
            }
            else if (timingDeltaNext <= BPS * (goodTimingWindow / 2f)) //Good
            {
                Debug.Log("*GOOD* timing info (NEXT BEAT): " + timingDeltaNext + "/" + BPS * (goodTimingWindow / 2f));
                quality = TimingQuality.Good;
            }

            if (quality != TimingQuality.Fail)
                lastSuccessfullBeat = GameMaster.nextBeatTimestamp;
        }

        if (quality == TimingQuality.Fail)
        {
            Debug.Log("*FAIL* timing info: (Last beat: " + timingDeltaLast + ", Next beat: " + timingDeltaNext + ")/" + BPS * (goodTimingWindow / 2f));
            GetComponent<MeshRenderer>().material.color = Color.red;
            CameraManager.instance.ShakeScreen(0.02f, 0.1f, 0.01f);
        }

        action(quality);
    }

    void MoveUp(TimingQuality quality)
    {
        if (quality != TimingQuality.Fail)
        {
            actualPosition += Vector3.forward * movementDistance;
        }
    }

    void MoveDown(TimingQuality quality)
    {
        if (quality != TimingQuality.Fail)
        {
            actualPosition += Vector3.back * movementDistance;
        }
    }

    void MoveRight(TimingQuality quality)
    {
        if (quality != TimingQuality.Fail)
        {
            actualPosition += Vector3.right * movementDistance;
        }
    }

    void MoveLeft(TimingQuality quality)
    {
        if (quality != TimingQuality.Fail)
        {
            actualPosition += Vector3.left * movementDistance;
        }
    }

    void OnBeat()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    void OnEnable()
    {
        GameMaster.OnBeat += OnBeat;
    }

    void OnDisable()
    {
        GameMaster.OnBeat -= OnBeat;
    }
}
