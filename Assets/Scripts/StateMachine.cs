﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateMachine
{
	public delegate void StateAction();

	private Stack<StateAction> states;

    public StateMachine()
    {
        states = new Stack<StateAction>();
    }

	public StateAction GetCurrentState()
	{
        if (states.Count == 0)
            return null;

        return states.Peek();
	}

	public void Cycle()
	{
		StateAction current = GetCurrentState();
		if(current != null)
			current();
	}

	public void PopState()
	{
		if(GetCurrentState() == null)
			return;

		states.Pop();
	}

	public void PushState(StateAction state, bool pop = false)
	{
		if(GetCurrentState() == state)
			return;
		
		if(pop)
			PopState();

		states.Push(state);
	}

	public void Clear()
	{
		states.Clear();
	}

    public Stack<StateAction> GetStatesStack { get { return states; } }
}
