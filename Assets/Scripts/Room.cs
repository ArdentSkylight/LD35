﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room : MonoBehaviour {

    [Header("Room Properties")]
    public List<UnitSpawnPoint> spawnPoints = new List<UnitSpawnPoint>();
    [Space]
    public Vector3 elevatorPosition;

    private List<Unit> spawnedUnits = new List<Unit>();

    public bool isClear
    {
        get
        {
            return spawnedUnits.Count == 0;
        }
    }


    public void SpawnEnemies()
    {
        if (spawnPoints.Count == 0)
        {
            Debug.LogWarning("Trying to spawn enemies but no spawnpoints exist.");
            return;
        }

        foreach (UnitSpawnPoint spawnPoint in spawnPoints)
        {
            Unit enemy = RoomManager.instance.GetRandomUnitPrefab();

            if (enemy != null)
            {
                enemy = Instantiate(enemy, spawnPoint.transform.position, Quaternion.identity) as Unit;
                spawnedUnits.Add(enemy);
            }
            else
            {
                Debug.LogWarning("Room.SpawnEnemies(): No enemy returned from RoomManager.GetRandomEnemyPrefab()");
                return;
            }
        }
    }

    void Update()
    {
        spawnedUnits.RemoveAll(item => item == null);
    }

    public void Load()
    {
        SpawnEnemies();
    }

    public void Unload()
    {
        foreach (Unit unit in spawnedUnits)
        {
            if (unit != null)
            {
                Destroy(unit.gameObject);
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0, 1, 1, 0.8f);
        Gizmos.DrawCube(transform.position + elevatorPosition, Vector3.one);
    }
}
