﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class Pathfinding : MonoBehaviour
{
    public class DuplicateKeyComparer<TKey>
                :
             IComparer<TKey> where TKey : IComparable
    {
        #region IComparer<TKey> Members

        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return 1;   // Handle equality as beeing greater
            else
                return result;
        }

        #endregion
    }

    public class SearchNode : IEquatable<SearchNode>
    {
        public Vector2 Pos;
        public float fromStart, toEnd;
        public SearchNode parent;
        public float GetTotal()
        {
            return fromStart*0.9f + toEnd;
        }
        public SearchNode()
        {
            parent = null;
        }
        public SearchNode(SearchNode n)
        {
            this.Pos = n.Pos;
            this.fromStart = n.fromStart;
            this.toEnd = n.toEnd;
            this.parent = n.parent;
        }
        public bool Equals(SearchNode sn)
        {
            if (this.Pos == sn.Pos)
            {
                return true;
            }
            else {
                return false;
            }
        }
    }


    public bool[,] listWalkable;
    public int terrainX = 5;
    public int terrainY = 5;
    public static Pathfinding instance;


	// Use this for initialization
	void Start ()
    {
        instance = this;

        listWalkable = new bool[63, 48];
        for (int y = 0; y < 46; y++)
            for (int x = 0; x < 63; x++)
                listWalkable[x, y] = true;


        //Top part
        for (int y = 0; y < 24; y++)
            for (int x = 0; x < 25; x++)
                listWalkable[x, y] = false;
        for (int y = 0; y < 24; y++)
            for (int x = 38; x < 63; x++)
                listWalkable[x, y] = false;
        for (int x = 0; x < 63; x++)
            listWalkable[x, 0] = false;

        //Big circle
        for (int y = 25; y < 26; y++)
            for (int x = 0; x < 24; x++)
                listWalkable[x, y] = false;
        for (int y = 25; y < 26; y++)
            for (int x = 40; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 26; y < 27; y++)
            for (int x = 0; x < 23; x++)
                listWalkable[x, y] = false;
        for (int y = 26; y < 27; y++)
            for (int x = 41; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 27; y < 29; y++)
            for (int x = 0; x < 22; x++)
                listWalkable[x, y] = false;
        for (int y = 27; y < 29; y++)
            for (int x = 42; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 29; y < 31; y++)
            for (int x = 0; x < 21; x++)
                listWalkable[x, y] = false;
        for (int y = 29; y < 31; y++)
            for (int x = 43; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 31; y < 47; y++)
            for (int x = 0; x < 20; x++)
                listWalkable[x, y] = false;
        for (int y = 31; y < 47; y++)
            for (int x = 44; x < 63; x++)
                listWalkable[x, y] = false;
        //Down part or circle
        for (int y = 39; y < 41; y++)
            for (int x = 0; x < 21; x++)
                listWalkable[x, y] = false;
        for (int y = 39; y < 41; y++)
            for (int x = 43; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 41; y < 43; y++)
            for (int x = 0; x < 22; x++)
                listWalkable[x, y] = false;
        for (int y = 41; y < 43; y++)
            for (int x = 42; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 43; y < 44; y++)
            for (int x = 0; x < 23; x++)
                listWalkable[x, y] = false;
        for (int y = 43; y < 44; y++)
            for (int x = 41; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 44; y < 45; y++)
            for (int x = 0; x < 24; x++)
                listWalkable[x, y] = false;
        for (int y = 44; y < 45; y++)
            for (int x = 41; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 45; y < 46; y++)
            for (int x = 0; x < 26; x++)
                listWalkable[x, y] = false;
        for (int y = 45; y < 46; y++)
            for (int x = 39; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 46; y < 47; y++)
            for (int x = 0; x < 28; x++)
                listWalkable[x, y] = false;
        for (int y = 46; y < 47; y++)
            for (int x = 37; x < 63; x++)
                listWalkable[x, y] = false;

        for (int y = 47; y < 48; y++)
            for (int x = 0; x < 63; x++)
                listWalkable[x, y] = false;

        FindClosestPath(new Vector3(1, 0, 1), new Vector3(3, 0, 3), new List<Vector3>());
    }

    public void FindClosestPath(Vector3 start, Vector3 target, List<Vector3> listPath)
    {
        Vector2 s = new Vector3(start.x, start.z);
        Vector2 e = new Vector3(target.x, target.z);

        //Check if the starting position is walkable, if not return empty path;
        if (!listWalkable[(int)s.x, (int)s.y])
        {
            return;
        }

        List<SearchNode> listSN = new List<SearchNode>();
        SortedList<float, SearchNode> open = new SortedList<float, SearchNode>((new DuplicateKeyComparer<float>()));
        List<SearchNode> closed = new List<SearchNode>();
        float toEnd = (int)(Mathf.Abs(s.x - target.x) + Mathf.Abs(s.y - target.z));

        SearchNode ns, na = new SearchNode(), temp = new SearchNode();
        temp.fromStart = 0;
        temp.toEnd = toEnd;
        temp.Pos.x = s.x;
        temp.Pos.y = s.y;
        temp.parent = null;
        //Initialize first node
        listSN.Add(temp);
        open.Add(listSN[0].GetTotal(), listSN[0]);
        ns = listSN[0];

        int breakCount = 0;
        while ((ns != null) &&
            (open.Count != 0) && //There is something to search on
            (ns.Pos != e))//And the current search node is not end
        {
            //Debug.Log("1:" + (ns != null));
            //Debug.Log("1:" + (open.Count != 0) + " " + ((int)open.Count).ToString());
            //Debug.Log("1:" + (ns.Pos != e) + ns.Pos.x.ToString() + " " + s.x.ToString() + " " + ns.Pos.y.ToString() + " " + s.y.ToString());

            breakCount++;
            if (breakCount > 100)
            {
                Debug.Log("breakCount:" + breakCount.ToString());
                break;
            }
            //Add nearby nodes
            //Up node
            //na.Pos = ns.Pos;
            na.Pos.x = ns.Pos.x;
            na.Pos.y = ns.Pos.y;
            na.Pos.y--;
            //Debug.Log("ns pos:" + ns.Pos.x.ToString() + " " + ns.Pos.y.ToString());
            if (ns.Pos.y > 0 && //Not out of bounds check
                !(closed.Contains(na)) &&
                (listWalkable[(int)na.Pos.x, (int)na.Pos.y]))
            {
                na.fromStart = ns.fromStart++;
                na.toEnd = (Mathf.Abs(na.Pos.x - e.x) + Mathf.Abs(na.Pos.y - e.y));
                na.parent = ns;
                //Debug.Log("Adding up to open" + na.Pos.x + " " + na.Pos.y + " " + na.GetTotal());
                listSN.Add(new SearchNode(na));
                open.Add(listSN.Last().GetTotal(), listSN.Last());
            }
            //Down node
            na.Pos.x = ns.Pos.x;
            na.Pos.y = ns.Pos.y;
            na.Pos.y++;
            //Debug.Log("ns pos:" + ns.Pos.x.ToString() + " " + ns.Pos.y.ToString());
            if (ns.Pos.y < terrainY-1 && //Not out of bounds check
                !(closed.Contains(na)) &&
                (listWalkable[(int)na.Pos.x, (int)na.Pos.y]))
            {
                na.fromStart = ns.fromStart++;
                na.toEnd = (Mathf.Abs(na.Pos.x - e.x) + Mathf.Abs(na.Pos.y - e.y));
                na.parent = ns;
                //Debug.Log("Adding down to open" + na.Pos.x + " " + na.Pos.y + " " + na.GetTotal());
                listSN.Add(new SearchNode(na));
                open.Add(listSN.Last().GetTotal(), listSN.Last());
            }
            //Left node
            na.Pos.x = ns.Pos.x;
            na.Pos.y = ns.Pos.y;
            na.Pos.x--;
            //Debug.Log("ns pos:" + ns.Pos.x.ToString() + " " + ns.Pos.y.ToString());
            if (ns.Pos.x > 0 && //Not out of bounds check
                !(closed.Contains(na)) &&
                (listWalkable[(int)na.Pos.x, (int)na.Pos.y]))
            {
                na.fromStart = ns.fromStart++;
                na.toEnd = (Mathf.Abs(na.Pos.x - e.x) + Mathf.Abs(na.Pos.y - e.y));
                na.parent = ns;
                //Debug.Log("Adding left to open" + na.Pos.x + " " + na.Pos.y + " " + na.GetTotal());
                listSN.Add(new SearchNode(na));
                open.Add(listSN.Last().GetTotal(), listSN.Last());
            }
            //Right node
            na.Pos.x = ns.Pos.x;
            na.Pos.y = ns.Pos.y;
            na.Pos.x++;
            //Debug.Log("ns pos:" + ns.Pos.x.ToString() + " " + ns.Pos.y.ToString());
            if (ns.Pos.x < terrainX-1 && //Not out of bounds check
                !(closed.Contains(na)) &&
                (listWalkable[(int)na.Pos.x, (int)na.Pos.y]))
            {
                na.fromStart = ns.fromStart++;
                na.toEnd = (Mathf.Abs(na.Pos.x - e.x) + Mathf.Abs(na.Pos.y - e.y));
                na.parent = ns;
                //Debug.Log("Adding right to open" + na.Pos.x + " " + na.Pos.y + " " + na.GetTotal());
                listSN.Add(new SearchNode(na));
                open.Add(listSN.Last().GetTotal(), listSN.Last());
            }

            closed.Add(ns);
            
            /*
            ////////////////////////////////////
            foreach (var v in open)
            {
                Debug.Log("b4 open = " + v.Value.Pos.x.ToString() + " " + v.Value.Pos.y.ToString());
            }
            
            ///////////////////////////////
            */

            //Removing

            
            for(int i = 0; i < open.Count; i++)
            {
                if(open.ElementAt(i).Value == ns)
                {
                    //Debug.Log("ns Removed" + open.ElementAt(i).Value.Pos.x + " " + open.ElementAt(i).Value.Pos.y + " key:" + open.ElementAt(i).Key + " " + open.ElementAt(i).Value.GetTotal());
                    //open.Remove(open.ElementAt(i).Key);
                    open.RemoveAt(i);
                    break;
                }
            }
            

            /*
            /////////////////////////////////////////
            foreach (var v in open)
            {
                Debug.Log("after open = " + v.Value.Pos.x.ToString() + " " + v.Value.Pos.y.ToString());
            }
            for (int i = 0; i < open.Count; i++)
            {
                Debug.Log("after open = " + open.ElementAt(i).Value.Pos.x.ToString() + " " + open.ElementAt(i).Value.Pos.y.ToString());
            }
            //open.Remove(ns);
            //////////////////////////////////////////////////
            */

            if (open.Count < 1)
            {
                //Debug.Log("ns = null");
                ns = null;
            }
            else
            {
                /*
                for (int i = 0; i < open.Count; i++)
                {
                    Debug.Log("Setting to " + open.ElementAt(i).Value.Pos.x.ToString() + " " + open.ElementAt(i).Value.Pos.y.ToString());
                    ns = open.ElementAtOrDefault(0).Value;
                    //ns = open.Values[0];
                    break;
                    //if (open.ElementAt(i).Value == ns)
                    //{
                    //    Debug.Log("ns Removed" + open.ElementAt(i).Value.Pos.x + " " + open.ElementAt(i).Value.Pos.y + " key:" + open.ElementAt(i).Key);
                    //    open.RemoveAt(i);
                    //    break;
                    //}
                }
                */
                Debug.Log("Setting to " + open.First().Value.Pos.x.ToString() + " " + open.First().Value.Pos.y.ToString() + " " + open.First().Value.GetTotal());
                //ns = open.First().Value;
                ns = open.First().Value;
            }
        }

        if (ns.Pos == e)
        {//Generate path
            //Debug.Log("Found path");
            int i = 0;
            while(ns != null)
            {
                i++;
                if (i == 100)
                    break;

                listPath.Add(new Vector3(ns.Pos.x, 0, ns.Pos.y));
                ns = ns.parent;
            }
            return;
        }
        else if(open.Count == 0)
        {//Searching path failed
            //Debug.Log("failed to find path");
        }
        /*
        foreach (var v in listSN)
        {
            Debug.Log("listSN = " + v.Pos.x.ToString() + " " + v.Pos.y.ToString());
        }
        foreach (var v in open)
        {
            Debug.Log("open = " + v.Value.Pos.x.ToString() + " " + v.Value.Pos.y.ToString());
        }
        foreach(var v in closed)
        {
            Debug.Log("closed = " + v.Pos.x.ToString() + " " + v.Pos.y.ToString());
        }
        */
    }

	// Update is called once per frame
	void Update ()
    {
	
	}
}
