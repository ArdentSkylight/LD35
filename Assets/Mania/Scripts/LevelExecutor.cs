﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelExecutor : MonoBehaviour {

    public LevelHandler levelHandler;
    public List<GameObject> flowingNotes = new List<GameObject>();
    Level currentLevel;
    int levelIndex = 0;

    public GameObject leftReceptor;
    public GameObject rightReceptor;
    public GameObject upReceptor;
    public GameObject downReceptor;
    public GameObject destroyReceptor;

    public GameObject leftNotePrefab;
    public GameObject rightNotePrefab;
    public GameObject upNotePrefab;
    public GameObject downNotePrefab;

    public Color highlightColor;
    public Color normalColor;

    // Use this for initialization
    void Start () {
        StartLevel(levelHandler.levels[levelIndex]);
	}

    void Update()
    {
        FlowNotes();
        CheckKeys();
    }

    void StartLevel(Level level)
    {
        levelIndex++;
        currentLevel = level;
        StartCoroutine(PlayLevel(level));
        PlayMusic(level);
    }

    IEnumerator PlayLevel(Level level)
    {
        InstantiateNote(level.notes[level.noteIndex].noteDirection);
        level.noteIndex++;
        yield return new WaitForSeconds(level.offsets[level.offsetIndex]);
        level.offsetIndex++;
        if (level.noteIndex != level.notes.Count)
        {
            StartCoroutine(PlayLevel(level));
        }
        else FinsishedLevel();
    }

    void InstantiateNote(Note.NoteDirection noteDirection)
    {
        switch (noteDirection)
        {
            case Note.NoteDirection.Left:
                GameObject obj = Instantiate(leftNotePrefab);
                obj.transform.SetParent(this.transform.FindChild("Left"), false);
                flowingNotes.Add(obj);
                break;
            case Note.NoteDirection.Right:
                GameObject obj2 = Instantiate(rightNotePrefab);
                obj2.transform.SetParent(this.transform.FindChild("Right"), false);
                flowingNotes.Add(obj2);
                break;
            case Note.NoteDirection.Up:
                GameObject obj3 = Instantiate(upNotePrefab);
                obj3.transform.SetParent(this.transform.FindChild("Up"), false);
                flowingNotes.Add(obj3);
                break;
            case Note.NoteDirection.Down:
                GameObject obj4 = Instantiate(downNotePrefab);
                obj4.transform.SetParent(this.transform.FindChild("Down"), false);
                flowingNotes.Add(obj4);
                break;
        }
    }

    void FlowNotes()
    {
        foreach (GameObject note in flowingNotes)
        {
            switch (note.name)
            {
                case "Left(Clone)":
                    note.GetComponent<RectTransform>().anchoredPosition += new Vector2(currentLevel.scrollSpeed * Time.deltaTime, 0);
                    break;
                case "Right(Clone)":
                    note.GetComponent<RectTransform>().anchoredPosition += new Vector2(-currentLevel.scrollSpeed * Time.deltaTime, 0);
                    break;
                case "Up(Clone)":
                    note.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, -currentLevel.scrollSpeed * Time.deltaTime);
                    break;
                case "Down(Clone)":
                    note.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, currentLevel.scrollSpeed * Time.deltaTime);
                    break;
                default:
                    Debug.Log("Fail in FlowNotes()");
                    break;
            }
            //if (note.GetComponent<RectTransform>().GetWorldCorners()))
            //{
            //    Destroy(note.gameObject);
            //}
        }
    }

    void FinsishedLevel()
    {
        Debug.Log("Level finished");
        SummarizeLevel();
    }

    void GotNote()
    {
        //get points 
    }

    void MissedNote()
    {
        //damage the player
    }

    void SummarizeLevel()
    {

    }

    void CheckKeys()
    {
        if (Input.GetKey(KeyCode.A))
            PressedKey(Note.NoteDirection.Left);
        else leftReceptor.GetComponent<Image>().color = normalColor;
        if (Input.GetKey(KeyCode.D))
            PressedKey(Note.NoteDirection.Right);
        else rightReceptor.GetComponent<Image>().color = normalColor;
        if (Input.GetKey(KeyCode.W))
            PressedKey(Note.NoteDirection.Up);
        else upReceptor.GetComponent<Image>().color = normalColor;
        if (Input.GetKey(KeyCode.S))
            PressedKey(Note.NoteDirection.Down);
        else downReceptor.GetComponent<Image>().color = normalColor;
    }

    void PressedKey(Note.NoteDirection noteDirection)
    {
        switch (noteDirection)
        {
            case Note.NoteDirection.Left:
                leftReceptor.GetComponent<Image>().color = highlightColor;
                break;
            case Note.NoteDirection.Right:
                rightReceptor.GetComponent<Image>().color = highlightColor;
                break;
            case Note.NoteDirection.Up:
                upReceptor.GetComponent<Image>().color = highlightColor;
                break;
            case Note.NoteDirection.Down:
                downReceptor.GetComponent<Image>().color = highlightColor;
                break;
        }
    }

    void PlayMusic(Level level)
    {
        GetComponent<AudioSource>().clip = level.music;
        GetComponent<AudioSource>().Play();
    }

}
