﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelHandler : MonoBehaviour {

    public List<Level> levels = new List<Level>();

}

[System.Serializable]
public class Level
{
    public string name;
    public int stepCount;
    public float scrollSpeed;
    public float difficulty;
    public int noteIndex;
    public AudioClip music;
    public List<float> offsets = new List<float>();
    public int offsetIndex;
    public List<Note> notes = new List<Note>();

    public Level(string scrollSpeed_, List<string> noteStrings, List<float> offsets_)
    {
        offsets = offsets_;
        scrollSpeed = int.Parse(scrollSpeed_);
        foreach (string str in noteStrings)
        {
            switch (str)
            {
                case "Left":
                    notes.Add(new Note(Note.NoteDirection.Left));
                    stepCount++;
                    break;
                case "Right":
                    notes.Add(new Note(Note.NoteDirection.Right));
                    stepCount++;
                    break;
                case "Up":
                    notes.Add(new Note(Note.NoteDirection.Up));
                    stepCount++;
                    break;
                case "Down":
                    notes.Add(new Note(Note.NoteDirection.Down));
                    stepCount++;
                    break;
                default:
                    Debug.Log("Failed to convert noteString to Note: " + str);
                    break;
            }
        }
    }

    public void CalculateDifficulty()
    {
        Debug.Log("Calculate");
    }
}

[System.Serializable]
public class Note
{
    public enum NoteDirection
    {
        Left,
        Right,
        Up,
        Down
    }

    public NoteDirection noteDirection;


    public Note(NoteDirection direction)
    {
        noteDirection = direction;
    }
}
