﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LevelFileReader : MonoBehaviour {

    public string levelDirectoryPath;
    FileInfo[] fileInfo;
    public LevelHandler levelHandler;

	// Use this for initialization
	void Start () {
        FindFiles();
        ReadFiles();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FindFiles()
    {
        DirectoryInfo info = new DirectoryInfo(Application.dataPath + levelDirectoryPath);
        fileInfo = info.GetFiles("*.txt");
    }

    void ReadFiles()
    {
        if (fileInfo != null)
        {
            for (int i = 0; i < fileInfo.Length; i++)
            {
                ReadFile(Application.dataPath + levelDirectoryPath + "/" + fileInfo[i].Name);
            }
        }
        else Debug.Log("Failed reading the files");
    }

    void ReadFile(string name)
    {

        StreamReader sr = new StreamReader(name);
        string fileContent = sr.ReadToEnd();
        sr.Close();
        if (fileContent == null)
        {
            Debug.Log("Couldn't read the .txt file");
            return;
        }
        TransformData(fileContent);
    }

    void TransformData(string content)
    {
        string[] lines = content.Split("\n"[0]);
        List<string> notes = new List<string>();
        List<float> offsets = new List<float>();
        for (int i = 1; i < lines.Length; i+=2)
        {
            notes.Add(lines[i].Trim());
        }
        for (int j = 2; j < lines.Length-1; j += 2)
        {
            offsets.Add(float.Parse(lines[j].Trim()));
        }

        levelHandler.levels.Add(new Level(lines[0], notes, offsets));
    }
}
